<?php
namespace app\controllers;
use yii\web\Controller;
use app\models\Pengguna;

class LoginController extends Controller {
    // http://domain4.test/?r=login/test
    function actionTest() {
        echo \Yii::$app->security->generatePasswordHash('1234');
        echo "Hello...";
    }

    function actionIndex() {
        return $this->render('login');
    }

    function actionAuth() { 
        $req = \Yii::$app->request;
        $user_id = $req->post('user_id');
        $password = $req->post('password');
        $pengguna = Pengguna::find()
        ->where(['user_id' => $user_id])->one();
        //var_dump($pengguna);
        if ($pengguna) {
            // pengguna wujud
            if (\Yii::$app->security
                ->validatePassword($password, $pengguna->password)) {
                // password ok
                \Yii::$app->user->login($pengguna, 60);
                return $this->redirect('index.php?r=login/dashboard');
            } else {
                // password x ok
                return $this->render('login');
            }
        } else { 
            // pengguna x wujud
            return $this->render('login');
        }
    }

    public function actionDashboard() {
        if (\Yii::$app->user->isGuest)
            return $this->redirect('/login/index');

        return $this->render('dashboard');
    }

    public function actionLogout() {
        \Yii::$app->user->logout(); // logout domain4
        return $this->redirect('http://sso.test/logout-sso'); // logout sso
    }

    // terima request dari SSO portal
    // index.php?r=login/accept
    public function actionAccept() {
        $req = \Yii::$app->request;
        $user_id = $this->hashString($req->get('user_id'), 'decrypt');
        $user = Pengguna::find()->where(['user_id' => $user_id])->one();
        if ($user) {
            \Yii::$app->user->login($user);
            return $this->redirect('/login/dashboard');
        } else {
            echo 'No permission..';
        }
    }

    //encrypt dan decrypt data
    function hashString($data, $method='encrypt') {
        $secret = '123456';
        $iv = substr(hash('sha256', $secret), 0, 16);

        if ($method == 'encrypt') {
            $str = openssl_encrypt($data, "AES-256-CBC", "$secret", 0, $iv);
        } else if ($method == 'decrypt'){
            $str = openssl_decrypt($data, "AES-256-CBC", "123456",0, $iv);
        }
        return $str;
    }
}
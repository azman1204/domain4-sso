<?php
namespace app\models;

class Pengguna extends \yii\db\ActiveRecord
implements \yii\web\IdentityInterface {
    public static function tableName() {
        return "users";
    }

    public static function findIdentity($id) {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return null;
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return true;
    }

    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }
}